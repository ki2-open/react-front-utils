import { useRef, useLayoutEffect } from "react";
import type { RefObject } from "react";

const isBrowser = typeof window !== `undefined`;

interface IGetScrollPositionProps {
  element?: RefObject<HTMLElement>;
  useWindow?: boolean;
}

interface IPosition {
  x: number;
  y: number;
}

type Timeout = ReturnType<typeof setTimeout>;

function getScrollPosition({
  element,
  useWindow,
}: IGetScrollPositionProps): IPosition {
  if (!isBrowser) return { x: 0, y: 0 };

  const target = element?.current || document.body;
  //const position = target.getBoundingClientRect();

  return useWindow
    ? { x: window.scrollX, y: window.scrollY }
    : { x: target.scrollLeft, y: target.scrollTop };
}

type EffectType = (currpos: IPosition, prevpos?: IPosition) => void;

export function useScrollPosition(
  effect: EffectType,
  deps?: Array<any>,
  element?: RefObject<HTMLElement>,
  useWindow?: boolean,
  wait?: number
) {
  const position = useRef(getScrollPosition({ useWindow }));

  const el = element?.current || window;

  let throttleTimeout: Timeout | null = null;

  const callBack = () => {
    const currPos = getScrollPosition({ element, useWindow });
    effect(currPos, position.current);
    position.current = currPos;
    throttleTimeout = null;
  };

  useLayoutEffect(() => {
    const handleScroll = () => {
      if (wait) {
        if (throttleTimeout === null) {
          // eslint-disable-next-line react-hooks/exhaustive-deps
          throttleTimeout = setTimeout(callBack, wait);
        }
      } else {
        callBack();
      }
    };

    el.addEventListener("scroll", handleScroll);

    return () => el.removeEventListener("scroll", handleScroll);
  }, deps);
}
