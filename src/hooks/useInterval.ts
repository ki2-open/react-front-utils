import { useEffect, useRef, useState } from "react";

export type CallbackType = ((stop: StopCallbackType) => void) | (() => void);
type StopCallbackType = () => void;

export const SECONDS = 1000;
export const MINUTES = 60 * SECONDS;
export const HOURS = 60 * MINUTES;

export function useInterval(callback: CallbackType, delay: number | null) {
  const savedCallback = useRef<typeof callback>();
  const [idelay, setIdelay] = useState<number | null>(delay);

  function handleStop() {
    setIdelay(null);
  }

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      if (savedCallback.current) {
        savedCallback.current(handleStop);
      }
    }
    if (idelay !== null) {
      let id = setInterval(tick, idelay);
      return () => clearInterval(id);
    }
  }, [idelay]);
}
